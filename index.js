const newUser = {
	email: "juan.delacruz@gmail.com",
	password: "thequickbrownfoxjumpsoverthelazydog"
}

// ACTIVITY:
const user = {
	firstName: "John",
	lastName: "Dela Cruz",
	age: 18,
	contactNumber: "09123456789",
	batchNumber: 235,
	email: "juan.delacruz@gmail.com",
	password: "sixteencharacters"
}

module.exports = {
	newUser: newUser,
	user: user
}
